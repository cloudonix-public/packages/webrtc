FROM cloudonix/chromium_depot_tools
# install additional tools to make depot_tools feel this is a workstation - its not very friendly to automation
ENV DEBIAN_FRONTEND=noninteractive
ADD apt.conf /etc/apt.conf.d/00noninteractive
RUN apt-get update && \
	apt-get install -qy sudo apt-utils rsync \
		openjdk-8-jdk tzdata && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*
ADD patches /src/patches
ARG GIT_COMMIT=c2c150bdf3a30d669b0d975e82c0829011888fa3
WORKDIR /build
RUN set -x; fetch --nohooks webrtc_android && \
	cd src && \
	./build/install-build-deps-android.sh --unsupported && apt-get clean && rm -rf /var/lib/apt/lists/* && \
	git checkout $GIT_COMMIT && \
	(yes | gclient sync --with_branch_heads --with_tags)
WORKDIR /build/src
RUN for file in /src/patches*; do patch -p1 -i $file; done
ADD build.sh /build/src/build.sh
RUN ./build.sh
