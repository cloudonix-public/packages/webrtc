#!/bin/bash
set -ex

. build/android/envsetup.sh || true

for arch in arm arm64 x86 x64; do
	args='is_component_build=false rtc_include_tests=false target_os="android" target_cpu="'$arch'"'
	[ "$arch" == "x64" ] && arch=x86_64 || true
	gn gen out/debug-$arch --args="$args is_debug=true"
	ninja -C out/debug-$arch
	mkdir -p /build/dist/debug/android/$arch
	find out/debug-$arch -name '*.a' -exec cp {} /build/dist/debug/android/$arch/ \;
	gn gen out/release-$arch --args="$args is_debug=false"
	ninja -C out/release-$arch
	mkdir -p /build/dist/release/android/$arch
	find out/release-$arch -name '*.a' -exec cp '{}' /build/dist/release/android/$arch/ \;
done

mkdir -p /build/dist/release/android/java
cp -r ./webrtc/modules/audio_device/android/java/* /build/dist/release/android/java
cp -r ./webrtc/rtc_base/java/* /build/dist/release/android/java
mkdir -p /build/dist/debug/android/java
cp -r ./webrtc/modules/audio_device/android/java/* /build/dist/debug/android/java
cp -r ./webrtc/rtc_base/java/* /build/dist/debug/android/java

rsync -am --include '*.h' -f 'hide,! */' \
	--exclude=tools --exclude=third_party --exclude=testing --exclude=test \
	--exclude=build --exclude=sdk --exclude=out --exclude=examples --exclude=buildtools \
	./ /build/dist/release/android/headers/
rsync -am ./third_party/boringssl/src/include/ /build/dist/release/android/headers/
rsync -am ./third_party/opus/src/include/ /build/dist/release/android/headers/
tar -zcf /build/webrtc-release.tar.gz -C /build/dist/release ./

rsync -am --include '*.h' -f 'hide,! */' \
	--exclude=tools --exclude=third_party --exclude=testing --exclude=test \
	--exclude=build --exclude=sdk --exclude=out --exclude=examples --exclude=buildtools \
	./ /build/dist/debug/android/headers/
rsync -am ./third_party/boringssl/src/include/ /build/dist/debug/android/headers/
rsync -am ./third_party/opus/src/include/ /build/dist/debug/android/headers/
tar -zcf /build/webrtc-debug.tar.gz -C /build/dist/debug ./
